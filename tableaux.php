<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableaux</title>
</head>
<body>
<?php

function afficher($a){
    echo '<p>' . $a .'</p>';
}
//Exercise 1 & 5
$mois = [
1 =>'Janvier',
2 => 'fevrier',
3 => 'Mars',
4 =>'Avril',
5 => 'Mai',
6 => 'Juin',
7 => 'Juillet',
8 =>'Aout',
9 => 'Septembre',
10 => 'Octobre',
11 => 'Novembre',
12 => 'Decembre',
];
// var_dump($mois)
// Exercice 2
// afficher($mois[2]);

// //  Exercice 3 
// afficher($mois[5]);


// // Exercice 4
// $mois[7] = 'Août';

//Exercice 7
// $mois[13] = 'Fleurence'


// Exercice 8

// for($i = 1; $i <= 12; $i++){
//     afficher($i);
// }

// //Exercice 9
// for($i = 1; $i <= count($mois); $i++){
//     afficher($mois[$i]);
// }

//Exercice  10
// //Foreach with value
// foreach($mois as $value){
//     afficher($value);

// }
//Foreach with index and value
foreach($mois as $index => $value){
    // afficher($index);
    // afficher($value);
    afficher("Le mois de " . $value . ' est le numero '. $index);

}


?>

    
</body>
</html>