<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les Boucles</title>
</head>
<body>

<!-- Exercise 1 -->
<!-- Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

    l'afficher
    incrémenter de 1 -->

<?php
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   error_reporting(E_ALL);



echo"<h3>Les boucles</h3>";

for($a  = 0; $a < 10; $a++){
    echo "les boucles : $a <br>";
    echo "<br>";
}
echo"<br>";
echo"<br>";


// <!-- Exercise 2 -->  
// <!-- Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     incrémenter la première variable -->

// echo"<h3>ExerciceTwo</h3";
$var1 = 0;
$var2= rand(1,100);
echo "la deuxieme variable: " .  $var2 . "<br>";


while( $var1 < 20){
    echo "le resultat de multiplication:    " .$var1 * $var2."<br>";
    echo "increment de première variable:   " .$var1++ ."<br>";
    echo "<br>";


}
echo"<br>";
echo"<br>";

// Exercice 3 Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     décrémenter la première variable


$x = 100;
$y= rand(1,100);
echo"la deuxième variable: " . $y . "<br>";


while( $x >10){
    echo "le resultat de multiplication:    " .$x * $y."<br>";
    echo "decrement la première variable:   " .$x--."<br>";
    echo "<br>";



}
echo"<br>";
echo"<br>";


// Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     l'incrementer de la moitié de sa valeur

// echo"<h3>Exercice 4</h3";
$c = 1;



while( $c <10){
    echo "increment la moitie de valeur: ".$c."<br>";
    $c += ($c/2);
    echo"<br>";
   


}
echo"<br>";
echo"<br>";



    // Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

    // echo"<h3>Exercice 5</h3";

    $d = 1;

    for($d = 1; $d <= 15; $d++){
        echo $d . " : on y arrive presque <br>";
       
    }
    echo"<br>";
    echo"<br>";


    // Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...

    $e = 20;

    for($e = 20; $e >= 0; $e--){
    
     
        echo $e. " : c'est presque bon<br>";
       
      
    }

    echo"<br>";
    echo"<br>";



    // Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...
    $f = 1;
    for($f = 1; $f <=100; $f++){
        echo ($f+=15). " : on tient le bon bout<br>";
        if ($f == 96) { break; }
    
     
       
       
      
       
    }
    echo"<br>";
    echo"<br>";

    // Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
    $g = 200;
    for($g = 200; $g >=1; $g--){
        
        echo ($g-=12).": Enfin<br>";
        if ($g == 6) { break; }
        
    
     
       
       
      
    }


?>












    
</body>
</html>