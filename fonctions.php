<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les fonctions</title>
</head>
<body>
<?php
// Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function afficher($v){
    echo '<h3>' . $v . '</h3>';
}

$h = "Hello ";
$m = " World";

function concat($h,$m){
    return $h . $m;
}

$a = concat($h,$m);

afficher($a);


// Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

//     Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux

// $b = 5;
// $e = 6;

function details($b,$e){

if($b > $e){
    echo"<h4>$b est  plus grand que le $e</h4>";
} else if ($b < $e){
    echo"<h4>$b est moins grand que le $e</h4>";
} else  {
    echo "<h4>Les deux nombres sont identiques</h4>";

}
    
}
$f = details(6, 6);
afficher($f);

// Exercice 5 Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.

$g = 5;
$j = "World";

function concat1($g,$j){
    return $g . $j;
}

$l = concat1($g,$j);

afficher ($l)."<br>";

// Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".
$lname = "Parois ";
$fname = "Hazelle ";
$age = 27;

// $lname="";
// $fname="";
// $age="";

function concat2($lname, $fname, $age){
    return $lname . $fname . $age;
}

// concat2("Parois" , "Hazelle" , 27);
// echo $result;
afficher ("Bonjour " . $lname . $fname . ", tu as ". $age . " ans.");
echo "<br>";

// Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur

$genre = "";
$agefh = "";

function identification($genre, $agefh){
    if($genre=="homme" && $agefh >= 18){
        afficher("Vous êtes un homme et vous êtes majeur");
    }
    else if($genre=="homme" && $agefh < 18){
        afficher("Vouse êtes un homme et vous êtes mineur");


    }
    else if($genre == "femme" && $agefh >= 18){
        afficher("Vous êtes une femme et vous êtes majeur");
    }
    else if($genre == "femme" && $agefh < 18) {
        afficher("Vouse êtes une femme et vous êtes mineur");

    }



}
$q = identification("femme", 18);
echo $q."<br>";


// Exercice 8: Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. 
// Tous les paramètres doivent avoir une valeur par défaut.

function test($n = 1, $o, $p = 1){
    return $n + $o + $p;
}
$o = 1;
afficher(test(2, $o ,2)) ;



?>
    
</body>
</html>